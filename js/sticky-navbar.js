// When the user scrolls the page, execute myFunction 
window.onscroll = function() {myFunction()};

// Get the navbar
var navMain = document.getElementById("navMain");
var navBar = document.getElementById("navBar");

// Get the offset position of the navbar
var sticky = navBar.offsetTop;

// Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
function myFunction() {
  if (window.pageYOffset >= sticky) {
    navMain.classList.add("sticky")
  } else {
    navMain.classList.remove("sticky");
  }
}