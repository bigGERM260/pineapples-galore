var lat = 38;
var lng = -95;
var zoom = 4;

/*function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
        console.log("getLocation()  Lat: " + lat + "  Long: " + lng + "  Zoom: " + zoom)
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    console.log("showPosition()  Lat: " + position.coords.latitude + "  Long: " + position.coords.longitude + "  Zoom: " + zoom)
    lat = position.coords.latitude;
    lng = position.coords.longitude;
    console.log("showPosition()  Lat: " + lat + "  Long: " + lng + "  Zoom: " + zoom)
    map.setCenter(new google.maps.LatLng(lat, lng));

}*/

function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition, noPosition, {timeout: 10000});
    } else { 
      console.log("Geolocation is not supported by this browser.")
    }
  }
  
  function showPosition(position) {
    console.log("Latitude: " + position.coords.latitude + 
    "  Longitude: " + position.coords.longitude)
  }

  function noPosition(){
      console.log("noPosition(): Error")
  }

getLocation();

function initMap(){
    var location = {lat: lat, lng: lng};
    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: zoom,
        center: location
    });

    var marker = new google.maps.Marker({
        position: location,
        map: map
    })
}

$(document).ready(function(){
    getLocation()
});