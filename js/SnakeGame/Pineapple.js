import {inSnake, addSnakeSegment} from "./Snake.js"
import {randomGridPosition} from "./Grid.js"

let pineapplePosition = getRandomPineapplePosition()
const GROWTH_RATE = 3;

// update the pineapple xy-cordinates
export function update(){
    if (inSnake(pineapplePosition)) {
        addSnakeSegment(GROWTH_RATE)
        pineapplePosition = randomGridPosition();
    }
}

// this appends the pineapple to the gameboard
export function draw(gameBoard){
    const pineappleElement = document.createElement('div')
    pineappleElement.className = 'pineapple';
    pineappleElement.style.gridRowStart = pineapplePosition.x
    pineappleElement.style.gridColumnStart = pineapplePosition.y
    gameBoard.append(pineappleElement);
}

// This returns a new grid position, not currently occupied by the snake
function getRandomPineapplePosition(){
    let newPineapplePosition
    while (newPineapplePosition == null || inSnake(newPineapplePosition)){
        newPineapplePosition = randomGridPosition()
    }
    return newPineapplePosition
}
