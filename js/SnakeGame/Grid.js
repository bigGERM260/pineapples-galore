const GRID_SIZE = 25

// this generates a random grid position
export function randomGridPosition(){
    return {
        x: Math.floor(Math.random() * GRID_SIZE) + 1,
        y: Math.floor(Math.random() * GRID_SIZE) + 1
    }
}

// determines if the snake is off of the game board grid
export function offGameBoard(position){
    return position.x < 1 || position.x > GRID_SIZE 
        || position.y < 1 || position.y > GRID_SIZE
}