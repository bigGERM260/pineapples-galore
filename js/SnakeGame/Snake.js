import { getInputDirection } from "./Input.js"

export const SNAKE_SPEED = 7;

const snakeBody = [{x: 13, y: 13}]
let newSegments = 0;

// update the snake segments xy-cordinates
export function update(){
    addSegments();

    const inputDirection = getInputDirection();

    for (let segment = snakeBody.length - 2; segment >= 0; segment--){
        snakeBody[segment+1] = { ...snakeBody[segment]}
    }

    snakeBody[0].y += inputDirection.x;
    snakeBody[0].x += inputDirection.y;
}

export function draw(gameBoard){
    //console.log('draw snake');
    snakeBody.forEach(segment => {
        //console.log(segment.x + '-' + segment.y)
        const snakeElement = document.createElement('div')
        snakeElement.className = 'snake';
        snakeElement.style.gridRowStart = segment.x
        snakeElement.style.gridColumnStart = segment.y
        gameBoard.append(snakeElement);
    })
}

export function addSnakeSegment(growthRate){
    newSegments = growthRate;
}

function addSegments(){
    for (let i = 0; i < newSegments; i++){
        snakeBody.push({...snakeBody[snakeBody.length -1]})
    }

    newSegments = 0;
}

export function inSnake(pineapplePosition, {ignoreHead = false} = {}){

    return snakeBody.some((snakeSegment, index) => {
        if (ignoreHead && index === 0) return false;
        return equalPositions(pineapplePosition, snakeSegment)
    })
}

// This determines if the coordinates ar the same position on the grid
function equalPositions(coord1, coord2){
    return coord1.x == coord2.x && coord1.y == coord2.y
}

// this returns the head of the snake
export function getSnakeHead(){
    return snakeBody[0];
}

// this determines if the snake head ran into the snake body
export function snakeAteItself(){
    return inSnake(snakeBody[0], {ignoreHead: true})
}
