import {update as updateSnake, draw as drawSnake, SNAKE_SPEED, getSnakeHead, snakeAteItself} from "./Snake.js";
import {update as updatePineapple, draw as drawPineapple} from "./Pineapple.js";
import {offGameBoard} from "./Grid.js"

let previousTime = 0;
let gameOver = false;
const gameBoard = document.getElementById('game-board');

// this is the game loop
function main(currentTime){
    if (gameOver){ 
        alert('Game over!  You\'re really bad at this! Try again...')
        return location.reload()
    }

    window.requestAnimationFrame(main);
    const secondsSinceLastRender = (currentTime - previousTime) / 1000;

    if (secondsSinceLastRender < 1/SNAKE_SPEED) return;

    //console.log(secondsSinceLastRender)

    previousTime = currentTime;
    //console.log(currentTime);

    update();
    draw();
}

window.requestAnimationFrame(main);

// this updates the game board element positions
function update(){
    updateSnake();
    updatePineapple();
    checkForDeath()
}

// this redraws the game board and elements
function draw(){
    gameBoard.innerHTML = '';
    drawSnake(gameBoard);
    drawPineapple(gameBoard);
}

// this determines if the game is over
function checkForDeath(){
    gameOver = offGameBoard(getSnakeHead()) || snakeAteItself()
}