let inputDirection = {x: 0, y:0}
let previousInputDirection = {x:0, y:0}

// this adds an keydown event listener to the window
// and determines the what direction the snake should go
window.addEventListener('keydown', e => {
    //console.log(e.key);
    if (previousInputDirection.y == 0){ // ignore up/down, if previous input was along Y axis
        if (e.key == 'ArrowUp') inputDirection = {x: 0, y:-1};
        if (e.key == 'ArrowDown') inputDirection = {x:0, y:1};
    }
    
    if (previousInputDirection.x == 0){ // ignore left/right, if previous input was along X axis
        if (e.key == 'ArrowLeft') inputDirection = {x:-1, y:0};
        if (e.key == 'ArrowRight') inputDirection = {x:1, y:0};
    }
    
})

// this returns the snakes new direction
export function getInputDirection(){
    previousInputDirection = inputDirection;
    return inputDirection;
}