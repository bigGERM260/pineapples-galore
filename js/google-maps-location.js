var lat = 39.00;
var lng = -100.00;
var zoom = 0;

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
        console.log("getLocation()  Lat: " + lat + "  Long: " + lng + "  Zoom: " + zoom)
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    lat = position.coords.latitude;
    lng = position.coords.longitude;
    console.log("showPosition()  Lat: " + lat + "  Long: " + lng + "  Zoom: " + zoom)
    map.setCenter(new google.maps.LatLng(lat, lng));

}

getLocation()

function initMap(){
    var location 
    
    if (lat != null && lng != null ){
        location = {lat: lat, lng: lng} // Current location
        zoom = 4;
    }	
    else {
        location = {lat: 39.76, lng: -86.15} // america 
        zoom = 12;
    }
    
    console.log("initMap()  Lat: " + lat + "  Long: " + lng + "  Zoom: " + zoom)
    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: zoom, 
        center: location
    });
        
}
